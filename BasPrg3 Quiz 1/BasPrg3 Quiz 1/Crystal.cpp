#include "Crystal.h"
#include<iostream>
#include<string>
#include "Character.h"
using namespace std;


Crystal::Crystal()
{
	this->Crystals = 15;
}


Crystal::~Crystal()
{
}

void Crystal::pullitem(Character * collector)
{
	cout << "You pulled a Crystal!" << endl;
	collector->mCrys += this->Crystals;
}
