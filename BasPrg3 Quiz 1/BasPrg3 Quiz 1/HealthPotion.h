#pragma once
#include "Items.h"
#include <string>
#include <vector>
#include <iostream>

using namespace std;
class Character;
class HealthPotion :
	public Items
{
public:
	HealthPotion();
	~HealthPotion();
	void pullitem(Character *collector);
};

