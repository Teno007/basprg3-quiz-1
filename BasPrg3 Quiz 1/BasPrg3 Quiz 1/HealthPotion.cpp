#include "HealthPotion.h"
#include<iostream>
#include<string>
#include  "Character.h"
using namespace std;


HealthPotion::HealthPotion()
{
	this->Heal = 30;
}


HealthPotion::~HealthPotion()
{
}

void HealthPotion::pullitem(Character * collector)
{
	cout << "You pulled a Heal!" << endl;
	collector->mHp += this->Heal;
}
