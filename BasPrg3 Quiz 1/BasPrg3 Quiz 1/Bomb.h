#pragma once
#include "Items.h"
#include <string>
#include <vector>
#include <iostream>

using namespace std;
class Character;
class Bomb : public Items
{
public:
	Bomb();
	~Bomb();
	void pullitem(Character *collector);
};

