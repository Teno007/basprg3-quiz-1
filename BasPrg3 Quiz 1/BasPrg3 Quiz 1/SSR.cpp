#include "SSR.h"
#include<iostream>
#include<string>
#include "Character.h"

using namespace std;


SSR::SSR()
{
	this->RarityPoints = 50;
}


SSR::~SSR()
{
}

void SSR::pullitem(Character * collector)
{
	cout << "You pulled an R!" << endl;
	collector->mRp += this->RarityPoints;
}
