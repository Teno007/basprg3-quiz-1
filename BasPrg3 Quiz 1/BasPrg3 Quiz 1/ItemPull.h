#pragma once
#include <string>
#include <vector>
#include <iostream>

using namespace std;
class Character;
class Items;
class ItemPull
{
public:
	ItemPull();
	~ItemPull();
	vector<Items*> items;
	
	void getItem(Character *collector);
	void getItemFee(Character *collector);

};

