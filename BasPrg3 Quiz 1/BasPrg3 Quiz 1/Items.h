#pragma once
#include <string>
#include <vector>
#include <iostream>

using namespace std;
class Character;
class Items
{
public:
	Items();
	~Items();

	virtual void pullitem(Character *collector);

protected:
	int RarityPoints;
	int Crystals;
	int DealDMG;
	int Heal;
};