#include "Bomb.h"
#include "ItemPull.h"
#include<iostream>
#include<string>
#include "Character.h"

using namespace std;

Bomb::Bomb()
{
	this->DealDMG = 25;
}


Bomb::~Bomb()
{
}

void Bomb::pullitem(Character * collector)
{
	cout << "You pulled a Bomb!" << endl;
	collector->mHp -= this->DealDMG;
}
