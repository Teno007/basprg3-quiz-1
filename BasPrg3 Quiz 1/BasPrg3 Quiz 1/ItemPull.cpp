#include "ItemPull.h"
#include "Character.h"
#include "Items.h"
#include "R.h"
#include "SR.h"
#include "SSR.h"
#include "HealthPotion.h"
#include "Bomb.h"
#include "Crystal.h"


ItemPull::ItemPull()
{
	this->items.push_back(new SSR());
	this->items.push_back(new SR());
	this->items.push_back(new R());
	this->items.push_back(new HealthPotion());
	this->items.push_back(new Bomb());
	this->items.push_back(new Crystal());
}


ItemPull::~ItemPull()
{
}

void ItemPull::getItem(Character * collector)
{
	int randomitem = rand() % 100 + 1;
	if (randomitem == 0 && randomitem <= 1)
	{
		items[0]->pullitem(collector);
		collector->aSSR += 1;
	}
	else if (randomitem >= 2 && randomitem <= 10)
	{
		items[1]->pullitem(collector);
		collector->aSR += 1;
	}
	else if (randomitem >= 11 && randomitem <= 50)
	{
		items[2]->pullitem(collector);
		collector->aR += 1;
	}
	else if (randomitem >= 51 && randomitem <= 65)
	{
		items[3]->pullitem(collector);
		collector->aHealthPotion += 1;
	}
	else if (randomitem >= 65 && randomitem <= 85)
	{
		items[4]->pullitem(collector);
		collector->aBomb += 1;
	}
	else if (randomitem >= 85 && randomitem <= 100)
	{
		items[5]->pullitem(collector);
		collector->aC += 1;
	}

}

void ItemPull::getItemFee(Character * collector)
{
	collector->mCrys -= 5;
	collector->mPulls += 1;
}

