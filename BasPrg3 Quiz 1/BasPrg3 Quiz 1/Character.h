#pragma once
#include <string>
#include <vector>
#include <iostream>

using namespace std;

class Character
{
public:

	Character(int hp, int crys, int rp, int pulls, int r, int sr, int ssr, int healthpotion, int bomb, int crystals);
	~Character();
	void printstats();
	void getPrintConclusion();
	int mHp;
	int mCrys;
	int mRp;
	int mPulls;
	int aR;
	int aSR;
	int aSSR;
	int aHealthPotion;
	int aBomb;
	int aC;
};