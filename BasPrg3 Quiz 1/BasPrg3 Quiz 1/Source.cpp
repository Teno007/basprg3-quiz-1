#include <iostream>
#include <string>
#include <time.h>
#include <vector>
#include "Character.h"
#include "ItemPull.h"


using namespace std;

int main()
{
	srand(time(NULL));

	Character *player = new Character(100,100,0,0,0,0,0,0,0,0);
	ItemPull *itempull = new ItemPull();

	while (player->mHp > 0 && player->mCrys > 0)
	{
		player->printstats();
		itempull->getItemFee(player);
		itempull->getItem(player);

		system("pause");
		system("cls");
	}
	cout << "Game over" << endl;
	player->getPrintConclusion();
	system("pause");
	return 0;
}