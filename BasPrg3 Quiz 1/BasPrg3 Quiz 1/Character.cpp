#include "Character.h"

Character::Character(int hp, int crys, int rp, int pulls, int r, int sr, int ssr, int healthpotion, int bomb, int crystals)
{
	mHp = hp;
	mCrys = crys;
	mRp = rp;
	mPulls = pulls;
	// a = amount 
	aR = r;
	aSR = sr;
	aSSR = ssr;
	aHealthPotion = healthpotion;
	aBomb = bomb;
	aC = crystals;
}

Character::~Character()
{
}

void Character::printstats()
{
	cout << "HP: " << mHp << endl;
	cout << "Crystals: " << mCrys << endl;
	cout << "Rarity Points: " << mRp << endl;
	cout << "Pulls: " << mPulls << endl;
}

void Character::getPrintConclusion()
{
	cout << "Items Pulled" << endl;
	cout << "--------------------" << endl;
	cout << "R x" << aR << endl;
	cout << "Health Potion x" << aHealthPotion << endl;
	cout << "SR x" << aSR << endl;
	cout << "Bomb x" << aBomb << endl;
	cout << "Crystals x" << aC << endl;
}

